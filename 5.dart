import 'dart:io';

void main() {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };

  var order = <String>[];

  for (var i = 0; i < 3; i++) {
    stdout.write('Enter the name of pizza ${i + 1}: ');
    var pizzaName = stdin.readLineSync();
    order.add(pizzaName?.trim().toLowerCase() ?? '');
  }

  double calculateTotalPrice(Map<String, double> prices, List<String> items) {
    var total = 0.0;
    for (var item in items) {
      final price = prices[item];
      if (price != null) {
        total += price;
      } else {
        print('$item pizza is not on the menu');
      }
    }
    return total;
  }

  var totalPrice = calculateTotalPrice(pizzaPrices, order);
  print('Total Price: \$${totalPrice.toStringAsFixed(2)}');
}
