import 'dart:io';

void main() {
  var values = [1, 3, 5, 7, 9];
  var sum = 0;

  for (var num in values) {
    sum += num;
  }

  print('Sum: $sum');
}
