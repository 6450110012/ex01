  import 'dart:io';
void  main(List<String> arguments) {
  int? netSalary, expenses;
  print("Enter your net salary... ");
  netSalary = int.parse(stdin.readLineSync()!);
  print("Enter your expenses... ");
  expenses = int.parse(stdin.readLineSync()!);

  if (netSalary > expenses) {
    print("You have saved ${netSalary - expenses} this month.");
  }
  else if (expenses > netSalary) {
    print("You have lost ${expenses - netSalary} this month.");
  }
  else {
    print("Your balance hasn't changed.");
  }

}