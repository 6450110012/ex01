void main() {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };
  const order = ['margherita', 'pepperoni', 'pineapple'];

  double calculateTotal(Map<String, double> prices, List<String> items) {
    var total = 0.0;
    for (var item in items) {
      final price = prices[item];
      if (price != null) {
        total += price;
      }
    }
    return total;
  }

  var total = calculateTotal(pizzaPrices, order);
  print('Total: \$${total.toStringAsFixed(2)}');
}

double calculateTotal(Map<String, double> prices, List<String> items) {
  var total = 0.0;
  for (var item in items) {
    final price = prices[item];
    if (price != null) {
      total += price;
    }
  }
  return total;
}
