void main(List<String> arguments) {
  print("#1 Variables");

  double temperature = 20;

  int value = 2;
  String pizza = 'pizza';
  String pasta = 'pasta';

  print("The temperature is ${temperature.toInt()}C");
  print("$value plus $value makes ${value += value}");
  print("I like $pizza and $pasta");

  print("\n"); }